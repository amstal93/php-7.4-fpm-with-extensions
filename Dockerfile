FROM php:7.4.25-fpm
RUN apt-get update && apt-get -y upgrade
RUN docker-php-ext-install pdo pdo_mysql
